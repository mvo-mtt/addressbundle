<?php
declare(strict_types=1);

namespace Mtt\AddressBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\MappedSuperclass()
 */
class Address
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $country;

    /**
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $postcode;

    /**
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $state;

    /**
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $city;

    /**
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $district;

    /**
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $street;

    /**
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $number;


    public function getId(): ?int
    {
        return $this->id;
    }


    public function getCountry(): ?string
    {
        return $this->country;
    }


    public function setCountry(string $country)
    {
        $this->country = $country;
    }


    public function getPostcode(): ?string
    {
        return $this->postcode;
    }


    public function setPostcode(string $postcode)
    {
        $this->postcode = $postcode;
    }


    public function getState(): ?string
    {
        return $this->state;
    }


    public function setState(string $state)
    {
        $this->state = $state;
    }


    public function getCity(): ?string
    {
        return $this->city;
    }


    public function setCity(string $city)
    {
        $this->city = $city;
    }

    public function getDistrict(): ?string
    {
        return $this->district;
    }


    public function setDistrict(string $district)
    {
        $this->district = $district;
    }


    public function getStreet(): ?string
    {
        return $this->street;
    }


    public function setStreet(string $street)
    {
        $this->street = $street;
    }


    public function getNumber(): ?string
    {
        return $this->number;
    }


    public function setNumber(string $number): ?string
    {
        $this->number = $number;
    }

    public function __toString()
    {
        return $this->getCountry() . ' ' . $this->getState() . ' ' . $this->getDistrict() . ' ' . $this->getCity() . ' ' . $this->getStreet() . ' ' . $this->getNumber();
    }
}